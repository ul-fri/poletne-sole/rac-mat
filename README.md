# Računalništvo sreča matematiko

Cilj delavnice je predstaviti nekaj zanimivih matematičnih problemov iz
različnih tematskih sklopov, pri reševanju katerih bo nujna uporaba
računalniških orodij.

## Kje, kdaj, kako

 * Na [Fakulteti za računalništvo in informatiko, Univerza v Ljubljani](http://www.fri.uni-lj.si)
 * od **6. julija** do **10. julija** med **9h in 15h**
 * 18 udeležencev, 5 predavataljev in še kakšen pomočnik
 * v računalniški učilnici **Pr11**

## Program

Vsak dan se bomo lotili neke teme. Izmenično bomo 

* zganjali matematiko v zvezke in na tablo
* programirali v [programskem jeziku Python](https://www.python.org/)

### Teorija iger

### Matematično modeliranje in numerična matematika

### Verjetnost, statistika in kriptografija

